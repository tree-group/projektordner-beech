# Wichtige Hinweise zum Projekt

Hier bitte alles vermerken was bei der Benutzung des Projekts besonders beachtet werden muss.

Insbesondere alle Abweichungen von der [Standard-Workflowanweisung](https://beech.li/workflow) müssen hier dokumentiert werden. Darunter zählen u.a. LUTs, Fonts, Plugins und Eigenheiten im Workflow, wie spezielle Roundtrips.

## Besonderheiten

### Workflow 

### Ordnerstruktur

### Plugins

### Assets