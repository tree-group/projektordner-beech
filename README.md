# Projektordner

Projektordner beginnen mit Jahr und Monat im Format YYMM dann Unterstrich, dann den Projektnamen. z.B.: **1702_AMD-SUED**

```
YYMM_Projektname
00_OUTPUT
  _DELIVERY - Die finalen Daten die der Kunde veröffentlicht
  VERSIONS - Alle Vorversionen
10_PROJECTS - Projektdateien, FCPX Mediatheken. Für andere Apps je einen Unterordner anlegen
20_FOOTAGE
  10_SHOOT - Gefilmtes Material
  20_SOUND - Projektspezifische Musik, SFX, Voiceover in Unterordnern  (Keine Libraries!)
  30_GRAPHICS - Logos, Stills, Panoramas in Unterordnern
  40_RENDERED - Daten die für das Projekt gerendert wurden, z.B. Mograph, Grades, 3D Elemente
  50_STOCK - Stock-Footage
  60_LUTs - Benutzte Lookup-Tables
30_DOCUMENTS -Relevante Dokumente für Post Production. Drehbücher, Änderungslisten, CI/CD
```

### Weitere mögliche Top Level Ordner:
```
11_XML-Import
12_XML-Export
21_FOOTAGE_YYMM (wird ein altes Projekt wiederbelebt, neues Footage in neuen Ordner)
60_LUTs
```

## Erlaubte Zeichen / Sonderzeichen

Erlaubt: ```A-Z a-z 0-9 - _ . ( )```   *Umlaute vermeiden*

Verboten: ```\ / : * ? " < > | [ ] = % $ + ; ~```

## Besonderheiten

Alle Abweichungen und Besonderheiten müssen in der Datei **WICHTIGE-HINWEISE.md** vermerkt werden.