# Footage vorbereiten und importieren
## Kopieren auf den Server
**ALLE** Daten die in den Schnitt kommen **MÜSSEN** auf den Server kopiert werden! 

**NIEMALS** direkt von einem lokalen Ordner bearbeiten.

### Aufnahmen
- In **20_FOOTAGE** sinnvolle Ordner anlegen, z.B Drehtag, Thema oder Ort
- ==Alle MP4, AVCHD, MXF Dateien von Sonys, Canons usw.== werden mit **EditReady** kopiert und umbenannt. (Ggf Metadaten wie camera angle hinzufügen)
- Dateinamen müssen eindeutig sein. z.B. Format: **YYMM\_Projekt\_Cam1-1\_00001.ext**

**RED** und **ARRI** Footage hat schon eindeutige Namen und sollte nicht umbenannt werden.

### Stock Footage
Zu jedem verwendeten Stock Footage muss ein Link zum Clip auf den entsprechenden Stock-Webseite vorhanden sein. Weblink wie Footage benennen. An Previews mit Wasserzeichen Suffix _preview anhängen. [FCPX Workflow beachten](https://www.youtube.com/watch?v=nuGeHRnxOn4)!

Datei | Link
------|------
StockLensflare444455.mp4 | StockLensflare444455.html
StockGedudel_preview.mp3 | Stockgedudel_Premiumbeat.html
StockBanana123_preview.mov | StockBanana123.html
