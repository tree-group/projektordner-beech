# Projektanlage Final Cut Pro X

## WICHTIGE Voreinstellungen
In den FCPX Einstellungen bei **Wiedergabe/Playback** den *Background Render* und *Optimierte Medien für Multicam Clips generieren* **ausschalten**.

Bei den **Import** Voreinstellungen die Option *Dateien am ursprünglichen Ort belassen* auswählen!

## Medienspeicherort und Cache
Beim Öffnen der Template Mediathek fragt FCPX nach dem Medienspeicherort, hier den richtigen /**Projekt/20\_FOOTAGE/** Ordner auswählen. Cache und Final Cut Backups sind voreingestellt.

## Ereignisse
Ereignisse sind von Projekt zu Projekt verschieden, oft reichen die voreingestellten Events **Media** und **Projekte**.

## Rollen
**Rollen müssen direkt beim ersten Import vergeben werden**. Mindestens folgende müssen immer stimmen

**Filme**: Video, Title, Stock Video, Stock Video PREVIEW (rot!)

**Sound**: Music, Effects, VO (ggf +Sprache) + VO PREVIEW (rot!), Stock Music, Stock Music PREVIEW (rot!)

## Schlagwörter
Schlagwörter beschreiben den Inhalt der Medien. Technische Details sollten in den Metadaten stehen.

## Stock Footage Previews in Final Cut
**BEVOR** Stock Footage (Music/Video) mit Wasserzeichen in die Timeline gezogen wird, einen zusammengesetzten (Compound) Clip daraus machen und diesen benutzen, benannt nach der Datei. [Siehe Tutorial](https://www.youtube.com/watch?v=nuGeHRnxOn4).

## Multicam
Wird mit mehreren Kameras mit sync Audio, z.B. ein Interview, gefilmt, immer Multicam Clips machen.

## Versionierung in Final Cut Pro X
Versionsnummern in Final Cut sollen mit der jeweiligen gerenderten Kundenversion übereinstimmen.

**Projekte immer als Schnappschuss duplizieren** (CMD+SHIFT+D) und am ursprünglichen Projekt weiterarbeiten. [Siehe Tutorial](https://www.youtube.com/watch?v=-Ae_t3qNkGc).


## Final Cut Pro X Proxy Workflow auf Arbos
Final Cut erstellt Proxies immer im Medienspeicherort den man angibt. Leider kann man Proxies nicht ohne weiteres wo anders erstellen oder verschieben, es gibt aber einen Workaround. [Siehe Tutorial](https://www.youtube.com/watch?v=AjW5-PTN70c).

Im **/zzz_CACHE/** Ordner einen Unterordner für das Projekt erstellen (z.B. /1706\_Projekt\_proxy/)

In der Mediathek den Medienspeicherort vorübergehend auf den neuen Ordner in zzz_CACHE ändern

Die Proxies generieren

Medienspeicherort zurück auf **(/20_FOOTAGE)** einstellen
